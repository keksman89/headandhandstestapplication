package developer.ink1804.headandhandstestapplication.presentation.auth

import com.arellomobile.mvp.MvpView

interface AuthView : MvpView {

    fun startLogin()
    fun finishLogin()

    fun showInputError(emailError: Int?, passwordError: Int?)
    fun hideInputError()

    fun showMessage(message: String)

    fun successLogin(message: String)
    fun failedLogin(message: String)
}