package developer.ink1804.headandhandstestapplication.presentation.auth

import com.arellomobile.mvp.InjectViewState
import developer.ink1804.headandhandstestapplication.AppApplication
import developer.ink1804.headandhandstestapplication.R
import developer.ink1804.headandhandstestapplication.common.Utils.applySchedulers
import developer.ink1804.headandhandstestapplication.common.extension.checkForEmail
import developer.ink1804.headandhandstestapplication.common.extension.checkForPassword
import developer.ink1804.headandhandstestapplication.network.api.WeatherMapApiManager
import developer.ink1804.headandhandstestapplication.presentation.BasePresenter
import javax.inject.Inject

@InjectViewState
class AuthPresenter : BasePresenter<AuthView>() {

    @Inject
    lateinit var apiManager: WeatherMapApiManager

    init {
        AppApplication.instance.applicationComponent.inject(this)
    }

    fun login(email: String, password: String) {

        var emailError: Int? = null
        var passwordError: Int? = null

        if (!email.checkForEmail()) {
            emailError = R.string.error_email_incorrect
        }

        if (!password.checkForPassword()) {
            passwordError = R.string.error_invalid_password
        }

        if (emailError != null || passwordError != null) {
            viewState.showInputError(emailError, passwordError)
            return
        }

        viewState.startLogin()
        loginRequest()
    }

    private fun loginRequest() {
        val apiDisposable = apiManager.getWeather("London")
                .compose(applySchedulers())
                .map { v -> "Температура в ${v.name}: ${v.main.currentTemp}K" }
                .subscribe({
                    viewState.successLogin(it)
                    viewState.finishLogin()
                }, {
                    viewState.showMessage(it.message.toString())
                    viewState.finishLogin()
                })

        disposeOnDestroy(apiDisposable)
    }
}