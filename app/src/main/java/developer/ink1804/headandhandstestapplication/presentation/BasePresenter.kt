package developer.ink1804.headandhandstestapplication.presentation

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BasePresenter<View : MvpView> : MvpPresenter<View>() {
    private val disposeBag = CompositeDisposable()

    protected fun disposeOnDestroy(subscription: Disposable) {
        disposeBag.add(subscription)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.clear()
    }
}