package developer.ink1804.headandhandstestapplication.di.component

import dagger.Component
import developer.ink1804.headandhandstestapplication.di.annotation.ApplicationScope
import developer.ink1804.headandhandstestapplication.di.module.application.WeatherMapApiModule
import developer.ink1804.headandhandstestapplication.network.api.WeatherMapApiManager
import developer.ink1804.headandhandstestapplication.presentation.auth.AuthPresenter

@ApplicationScope
@Component(modules = [WeatherMapApiModule::class])
interface ApplicationComponent {
    fun getApiManager(): WeatherMapApiManager

    fun inject(authPresenter: AuthPresenter)
}
