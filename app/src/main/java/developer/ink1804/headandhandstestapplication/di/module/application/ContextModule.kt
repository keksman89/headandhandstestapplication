package developer.ink1804.headandhandstestapplication.di.module.application

import android.content.Context
import dagger.Module
import dagger.Provides
import developer.ink1804.headandhandstestapplication.di.annotation.ApplicationContext
import developer.ink1804.headandhandstestapplication.di.annotation.ApplicationScope

@Module
class ContextModule(var context: Context) {

    @ApplicationContext
    @ApplicationScope
    @Provides
    fun context(): Context {
        return context.applicationContext
    }
}