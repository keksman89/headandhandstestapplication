package developer.ink1804.headandhandstestapplication.di.module.application

import android.content.Context
import dagger.Module
import dagger.Provides
import developer.ink1804.headandhandstestapplication.di.annotation.ApplicationContext
import developer.ink1804.headandhandstestapplication.di.annotation.ApplicationScope
import okhttp3.Cache
import okhttp3.ConnectionPool
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit


@Module(includes = [ContextModule::class])
class OkHttpClientModule {

    @Provides
    fun okHttpClient(cache: Cache, httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient().newBuilder()
                .cache(cache)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .connectionPool(ConnectionPool(0, 1, TimeUnit.MICROSECONDS))
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(queryInterceptor())
                .build()
    }

    @Provides
    fun cache(cacheFile: File): Cache {
        return Cache(cacheFile, 10 * 1000 * 1000)
    }

    @ApplicationScope
    @Provides
    fun file(@ApplicationContext context: Context): File {
        val file = File(context.cacheDir, "HttpCache")
        file.mkdir()
        return file
    }

    @Provides
    fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
            Timber.d(message)
        })
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }


    @Provides
    fun queryInterceptor(): Interceptor {
        return Interceptor {
            var request = it.request()
            var url = request.url()
            url = url.newBuilder().addQueryParameter("APPID", "6f5994fb24761941a9fdd63586805a48").build()
            request = request.newBuilder().url(url).build()
            it.proceed(request)
        }
    }

}