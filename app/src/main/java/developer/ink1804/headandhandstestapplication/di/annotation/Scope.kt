package developer.ink1804.headandhandstestapplication.di.annotation

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class ApplicationScope
