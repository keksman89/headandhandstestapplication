package developer.ink1804.headandhandstestapplication.network.pojo

import com.google.gson.annotations.SerializedName

class Main {

    @SerializedName("temp_min")
    lateinit var minTemp: String

    @SerializedName("temp_max")
    lateinit var maxTemp: String

    @SerializedName("temp")
    lateinit var currentTemp: String
}