package developer.ink1804.headandhandstestapplication.network.pojo.response

import com.google.gson.annotations.SerializedName
import developer.ink1804.headandhandstestapplication.network.pojo.BaseResponseModel
import developer.ink1804.headandhandstestapplication.network.pojo.Main

class WeatherParameters : BaseResponseModel() {

    @SerializedName("main")
    lateinit var main: Main

    @SerializedName("name")
    lateinit var name: String

}