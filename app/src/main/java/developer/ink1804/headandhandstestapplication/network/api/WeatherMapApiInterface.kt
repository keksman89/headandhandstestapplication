package developer.ink1804.headandhandstestapplication.network.api

import developer.ink1804.headandhandstestapplication.network.pojo.response.WeatherParameters
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherMapApiInterface {

    @GET("weather")
    fun getWeather(
            @Query("q") city: String): Observable<WeatherParameters>

}