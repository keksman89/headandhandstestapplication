package developer.ink1804.headandhandstestapplication.network.api

import developer.ink1804.headandhandstestapplication.network.pojo.response.WeatherParameters
import io.reactivex.Observable
import javax.inject.Inject

class WeatherMapApiManager @Inject constructor(private var apiInterface: WeatherMapApiInterface) : WeatherMapApiService {

    override fun getWeather(city: String): Observable<WeatherParameters> {
        return apiInterface.getWeather(city)
    }

}