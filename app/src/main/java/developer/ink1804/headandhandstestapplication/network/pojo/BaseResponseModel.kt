package developer.ink1804.headandhandstestapplication.network.pojo

import com.google.gson.annotations.SerializedName

open class BaseResponseModel {

    @SerializedName("message")
    var message: String = "Неизвестная ошибка"
}