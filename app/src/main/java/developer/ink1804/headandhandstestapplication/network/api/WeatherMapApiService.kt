package developer.ink1804.headandhandstestapplication.network.api

import developer.ink1804.headandhandstestapplication.network.pojo.response.WeatherParameters
import io.reactivex.Observable

interface WeatherMapApiService {

    fun getWeather(city: String): Observable<WeatherParameters>

}