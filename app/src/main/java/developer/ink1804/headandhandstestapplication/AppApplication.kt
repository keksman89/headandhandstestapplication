package developer.ink1804.headandhandstestapplication

import android.app.Application
import developer.ink1804.headandhandstestapplication.di.component.ApplicationComponent
import developer.ink1804.headandhandstestapplication.di.component.DaggerApplicationComponent
import developer.ink1804.headandhandstestapplication.di.module.application.ContextModule
import developer.ink1804.headandhandstestapplication.ui.AuthActivity
import timber.log.Timber

class AppApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    var activity: AuthActivity? = null

    companion object {
        lateinit var instance: AppApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        Timber.plant(Timber.DebugTree())

        applicationComponent = DaggerApplicationComponent.builder()
                .contextModule(ContextModule(this))
                .build()
    }
}