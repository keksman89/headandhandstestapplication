package developer.ink1804.headandhandstestapplication.common.extension

val EMAIL_REGEX = Regex("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
var PASSWORD_REGEX = Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}\$")

fun String.checkForEmail(): Boolean {
    return this.matches(EMAIL_REGEX)
}

fun String.checkForPassword(): Boolean {
    return this.matches(PASSWORD_REGEX)
}