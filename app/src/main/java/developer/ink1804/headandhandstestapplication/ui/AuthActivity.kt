package developer.ink1804.headandhandstestapplication.ui

import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import com.arellomobile.mvp.presenter.InjectPresenter
import developer.ink1804.headandhandstestapplication.AppApplication
import developer.ink1804.headandhandstestapplication.R
import developer.ink1804.headandhandstestapplication.common.extension.RxEditText.getTextWatcherObservable
import developer.ink1804.headandhandstestapplication.presentation.auth.AuthPresenter
import developer.ink1804.headandhandstestapplication.presentation.auth.AuthView
import developer.ink1804.headandhandstestapplication.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_auth.*
import kotlinx.android.synthetic.main.inc_toolbar.*

class AuthActivity : BaseActivity(), AuthView {

    @InjectPresenter
    lateinit var presenter: AuthPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        AppApplication.instance.activity = this
        initUI()
    }

    private fun initUI() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = 0
        }

        initToolbar()

        etPassword.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                tryLogin()
                true
            } else {
                false
            }
        }

        btLogin.setOnClickListener { tryLogin() }
    }

    override fun onResume() {
        super.onResume()

        val emailDisposable = getTextWatcherObservable(etEmail).subscribe {
            emailContainer.error = null
        }

        val passwordDisposable = getTextWatcherObservable(etPassword).subscribe {
            passwordContainer.error = null
        }

        disposeBag.addAll(emailDisposable, passwordDisposable)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = getString(R.string.auth_toolbar_title)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_auth, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }

        if (item.itemId == R.id.menu_item_create) {
            showMessage(getString(R.string.not_available))
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showMessage(message: String) {
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun tryLogin() {
        presenter.login(etEmail.text.toString(), etPassword.text.toString())
    }

    override fun startLogin() {
        showDialog()
    }

    override fun finishLogin() {
        hideDialog()
    }

    override fun showInputError(emailError: Int?, passwordError: Int?) {
        emailContainer.error = if (emailError == null) null else getString(emailError)
        passwordContainer.error = if (passwordError == null) null else getString(passwordError)
    }

    override fun hideInputError() {
        etEmail.error = null
        etPassword.error = null
    }

    override fun successLogin(message: String) {
        showMessage(message)
    }

    override fun failedLogin(message: String) {
        showMessage(message)
    }
}


