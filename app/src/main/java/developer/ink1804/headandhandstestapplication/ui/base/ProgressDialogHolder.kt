package developer.ink1804.headandhandstestapplication.ui.base

interface ProgressDialogHolder {

    fun createDialog()
    fun showDialog()
    fun hideDialog()
}