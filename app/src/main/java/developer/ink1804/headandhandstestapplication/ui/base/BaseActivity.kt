package developer.ink1804.headandhandstestapplication.ui.base

import android.app.Dialog
import android.support.v7.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatActivity
import developer.ink1804.headandhandstestapplication.R
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity : MvpAppCompatActivity(), ProgressDialogHolder {

    lateinit var disposeBag: CompositeDisposable
    private var dialog: Dialog? = null

    override fun createDialog() {
        if (dialog == null) {
            val builder = AlertDialog.Builder(this)
            builder.setView(R.layout.dialog_progress)
            builder.setCancelable(false)
            this.dialog = builder.create()
        }
    }

    override fun onResume() {
        super.onResume()
        disposeBag = CompositeDisposable()
    }

    override fun onPause() {
        super.onPause()
        disposeBag.dispose()
    }

    override fun showDialog() {
        if (dialog == null) {
            createDialog()
        }

        dialog!!.show()
    }

    override fun hideDialog() {
        if (dialog != null) {
            dialog!!.dismiss()
        }
    }
}